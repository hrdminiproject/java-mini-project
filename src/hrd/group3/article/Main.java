package hrd.group3.article;

import hrd.group3.article.layout.*;
import hrd.group3.article.processing.FileProcess;
import hrd.group3.article.processing.Pagination;
import hrd.group3.article.processing.TempFile;
import hrd.group3.article.processing.Validation;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
	// Welcome to Article Management Application
	// Group 3 of Phnom Penh Class
	//
	// Group member:
	// 1.YIN KOKPHENG
	// 2.PHAN PIRANG
	// 3.HO SYSOME
	// 4.OK REKSMEY
	// 5.SENG OUDAM
	public static ArrayList<Article> articleList = new ArrayList<Article>();

	public static void main(String[] args) {
		FileProcess.checkFile();// Check database file and id file

		File tempFile = TempFile.getTempFile();// get temporary file

		/*
		 * check for user want to recover their data when the application is
		 * unexpectedly shutdown"
		 */

		if (tempFile != null) {
			System.out
					.println("╔═════════════════════════════════════════════════════════════════════════════════════════════════════╗");
			System.out
					.println("║                                                                                                     ║");
			System.out
					.println("╟─────────────────── Message: Your application was unexpectedly shutdown!!!(☉ₒ☉) ────────────────────╢");
			System.out
					.println("║                                                                                                     ║");
			System.out
					.print("╚═════════════════════════════════════════════════════════════════════════════════════════════════════╝");

			if (!Validation.checkConfrim("recover your data")) {
				tempFile.delete();
				tempFile = null;
			}
		}
		Welcome.logo();

		/*
		 * Test adding record
		 * 
		 * Commment this two line of code after first run application
		 */

//		autoAddToList(10_000_000);
//		FileProcess.ThreadWriter writeFile = new FileProcess().new ThreadWriter(
//				articleList);
//		writeFile.start();

		/*
		 * Uncomment This code after first run application
		 */
		try {
			articleList = FileProcess.read();
		} catch (Exception e) {
		}
		
		Pagination pn = new Pagination(articleList);
		Layout layout = new Layout();

		/*
		 * If Failed in reading ID from FILE , get ID from arrayList size
		 * 
		 * If temporary file has data, it will read data from file when insert
		 * to collection then write to database file
		 */
		try {
			Article.MAX_ID = FileProcess.read_MAX_ID();
			TempFile.readFromTemporaryFile(tempFile);
		} catch (Exception e) {
			if (articleList.size() != 0) {
				// get last ID from ArrayList
				FileProcess.write_MAX_ID(articleList.get(0).getId());
				Article.MAX_ID = FileProcess.read_MAX_ID();
			}
		}

		// Display Header
		layout.header();

		// Show Table of Records
		pn.showPage(articleList);

		// Show Menu
		pn.navigation();

	}

	/************************** Test adding Function ******************************/

	/**
	 * Random data for add to collection
	 * 
	 */
	final static String[] Title = { "HRD", "Kosign", "Article", "Java", "SQL",
			"Sabay", "JavaScript", "VB.Net", "Ruby", "Advance C#", "HTML",
			"CSS" };
	final static String[] Author = { "Yin Kokpheng", "Pirang", "Ho Sysome",
			"Ok Reskmey", "Seng Oudam" };
	final static String[] Content = {
			"How to randomly pick an element from an array",
			"Random shuffling of an array",
			"How to generate a random alpha-numeric string?",
			"SQL Server 2005 freezes (because of application), need logging",
			"Join and update same column sql server",
			"C#: How to test for StackOverflowException",
			"Getting stackoverflowexception in ASP.NET MVC's Javascript Minifier at runtime" };
	static Random random = new Random();

	public static void autoAddToList(long amount) {
		for (int i = 1; i <= amount; i++) {
			articleList.add(new Article(Title[random.nextInt(Title.length)],
					Author[random.nextInt(Author.length)], Content[random
							.nextInt(Content.length)]));
		}
		Collections.sort(articleList, Collections.reverseOrder());
	}
}
