package hrd.group3.article;

import hrd.group3.article.processing.Pagination;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Article implements Comparable<Article>, Serializable {

	// Article fields
	static int MAX_ID;
	private int id;
	private String title, author, date, content;
	private static DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
	static String fullDate = dateFormat.format(new Date());

	/**
	 * Default Constructor.
	 */
	public Article() {
		this("Unknown", "Unknown", "Korean Software HRD Center");
	}

	/**
	 * Constructor for initializing an object. ID and date are automatically
	 * set.
	 * 
	 * @param title
	 *            Article's title.
	 * 
	 * @param author
	 *            Article's author.
	 * 
	 * @param content
	 *            Article's contents'
	 */
	public Article(String title, String author, String content) {
		this.id = ++MAX_ID;
		this.title = title;
		this.author = author;
		this.date = fullDate;
		this.content = content;
		Pagination.current_ID = MAX_ID;
	}

	/**
	 * This method is used for decrease auto number when user cancel.
	 */
	public void decreaseAutoNumber() {
		MAX_ID -= 1;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public String toString() {
		return (title + " " + author + " " + content).toLowerCase();
	}

	public String getArticle() {
		return (id + "\t" + title + "\t" + author + "\t" + date + "\t" + content);
	}

	@Override
	public int compareTo(Article o) {
		if (this.getId() > o.getId()) {
			return 1;
		} else if (this.getId() < o.getId()) {
			return -1;
		}
		return 0;
	}

}
