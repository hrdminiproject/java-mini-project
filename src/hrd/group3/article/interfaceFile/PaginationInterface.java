package hrd.group3.article.interfaceFile;

import hrd.group3.article.Article;

import java.util.List;

public interface PaginationInterface {

	// ***************************************************__Navagation__***************************************************************
	/**
	 * Show Navigation.
	 */
	public void navigation();

	// ***************************************************__Set_row_numbers__***************************************************************
	/**
	 * Set Row Number To Display.
	 */
	public void setRowNumber();

	// ***************************************************__Goto_Page__***************************************************************
	/**
	 * Go To Page Number.
	 */
	public void goToPage();

	// ***************************************************__Show_First_Page__***************************************************************
	/**
	 * Show Record on First page.
	 */
	public void showFirstPage();

	// ***************************************************__Show_Last_Page__***************************************************************
	/**
	 * Show Record on Last page.
	 */
	public void showLastPage();

	// ***************************************************__Show_Next_Page__***************************************************************
	/**
	 * Show Record on Next page.
	 */
	public void showNextPage();

	// ***************************************************__Show_Previous_Page__***************************************************************
	/**
	 * Show Record on Previous page.
	 */
	public void showPreviousPage();

	// ***************************************************__Show_Page__***************************************************************
	/**
	 * Show Record on page.
	 * 
	 * @param list
	 *            For display.
	 * 
	 */
	public void showPage(List<Article> list);

}
