package hrd.group3.article.interfaceFile;

import hrd.group3.article.Article;

import java.util.List;

public interface ManipulationInterface {

	// **************************************************__Add__***************************************************************
	/**
	 * This method is used for adding a new article to list.
	 * 
	 * @param list
	 *            The list contains article.
	 */
	public boolean add(List<Article> list);

	// **************************************************__Delete__****************************************************************
	/**
	 * This method is used for delete an article by id
	 * 
	 * @param list
	 *            The list that contain article will be deleted.
	 * 
	 * @param id
	 *            ID of article that will be deleted.
	 * 
	 * @return return the objects that has been remove.
	 * 
	 */
	public Article delete(List<Article> list, int id);

	// *************************************************__Update__*****************************************************************
	/**
	 * This method is used for update an article by id
	 * 
	 * @param list
	 *            target list.
	 * 
	 * @param id
	 *            ID of article that will be updated.
	 * 
	 * @return return the objects that has been update
	 * 
	 */
	public Article update(List<Article> list, int id);

	// ***************************************************__Search__***************************************************************
	/**
	 * This method is used for searching an article by id.
	 * 
	 * @param list
	 *            The list contains article.
	 * @param id
	 *            Object's id.
	 * 
	 * @return return an object that is found.
	 * 
	 */
	public Article search(List<Article> list, int id);

	// ****************************************************__Display_Information__**************************************************************
	/**
	 * This method is used for display an article info and it will show an
	 * article ID, Title, Author, Contents.
	 * <p>
	 * It will show an not found message if there is null object.
	 * 
	 * @param article
	 *            an article object.
	 */
	public void displayInfo(Article article);
}
