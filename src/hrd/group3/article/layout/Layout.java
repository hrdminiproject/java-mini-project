package hrd.group3.article.layout;

import hrd.group3.article.Article;

import java.util.Iterator;
import java.util.List;

public class Layout {
	public static boolean showTip = false;

	// ************************************************************__Header__********************************************************************************
	/**
	 * This method is used for display header and it contains Center's Name,
	 * Title, Group, Class, and Members's Name.
	 */
	public void header() {
		//
		System.out
				.println("╔═════════════════════════════════════════════════════════════════════════════════════════════════════╗");
		System.out
				.println("║                                                                                                     ║");
		System.out
				.println("╟──────────────────────────────────────KOREA SOFTWARE HRD CENTER──────────────────────────────────────╢");
		System.out
				.println("║                                                                                                     ║");
		System.out
				.println("╚═════════════════════════════════════════════════════════════════════════════════════════════════════╝");
		System.out
				.println("                              *Welcome to Article Management Application*");
		System.out
				.println("                                      Group 3 of Phnom Penh Class");
		System.out
				.println("                                            1.YIN KOKPHENG");
		System.out
				.println("                                            2.PHAN PIRANG");
		System.out
				.println("                                            3.HO SYSOME");
		System.out
				.println("                                            4.OK REKSMEY");
		System.out
				.println("                                            5.SENG OUDAM");

		System.out
				.println("*─────────────────────────────────────────────────────────────────────────────────────────────────────*");
	}

	// ***************************************************************__Body__*****************************************************************************

	/**
	 * This method is used for display content in the list of Article.
	 * 
	 * @param list
	 *            The list which contains article that is going to display.
	 * 
	 * @param start
	 *            Start represents to start number of article's ID.
	 * 
	 * @param stop
	 *            Stop represents to stop number of article's ID.
	 * 
	 * @param currentPage
	 *            Represent to the page that user have selected.
	 * 
	 * @param totalPage
	 *            Represent to total page that display.
	 */
	public void Body(List<Article> list, int start, int stop, int currentPage,
			int totalPage) {
		String leftAlignFormat = "║ %-10s | %-31s | %-32s | %-18s║%n";
		System.out
				.format("╔═════════════════════════════════════════════════════════════════════════════════════════════════════╗%n");
		System.out
				.printf("║     NO     |              TITLE              |              AUTHOR              |        DATE       ║%n");
		System.out
				.format("╠═════════════════════════════════════════════════════════════════════════════════════════════════════╣%n");

		// check list's size then read all the contents in list to display.
		if (list.size() != 0) {
			Iterator<Article> itr = list.subList(start, stop).iterator();
			Article obj;
			while (itr.hasNext()) {
				obj = itr.next();
				System.out.format(leftAlignFormat, obj.getId(), (obj.getTitle()
						.length() > 20) ? obj.getTitle().substring(0, 20)
						+ "..." : obj.getTitle(),
						(obj.getAuthor().length() > 20) ? obj.getAuthor()
								.substring(0, 20) + "..." : obj.getAuthor(),
						obj.getDate());
				System.out
						.println("╟─────────────────────────────────────────────────────────────────────────────────────────────────────╢");
			}
		}
		// if no record in list.
		else {
			System.out
			.println("╟────────────────────────────────────────── No records !! ────────────────────────────────────────────╢");
			currentPage = 0;
			totalPage = 0;
		}

		// display current page, total page, and total records.
		//System.out				.println("║║");
		System.out.format("║ %-34s %64s ║%n", "Page: " + currentPage + "/"
				+ totalPage, "Total Records: "
				+ list.size());
		System.out
				.println("╚═════════════════════════════════════════════════════════════════════════════════════════════════════╝");

	}

	// *******************************************************************__Footer__*************************************************************************

	/**
	 * This method is used for display footer and it contents Menu, and some
	 * tips to guide user.
	 */
	public void footer() {
		System.out
				.println("│=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-<<<Menu>>>-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-==-=-=-=-==│");
		
		String leftAlignFormat = "│  %-6s | %-6s | %-7s| %-6s | %-6s | %-6s | %-6s │%n";

		System.out.format(leftAlignFormat,  "1:(F)irst", "2:(P)rev", "3:(N)ext",
				"4:(L)ast", "5:Sho(w) All", "6:Show/Hide (T)ips","7:(B)ackup/Restore");
		
		String leftAlignFormat2 = "│%-13s %-6s | %-5s | %-8s | %-8s | %-8s | %-23s │%n";
		System.out.format(leftAlignFormat2, "", "8:(R)ead", "9:(A)DD", "10:(S)earch",
				"11:(U)pdate", "12:(D)elete", "13:(G)oto");
		String leftAlignFormat3 = "│%-25s %-6s | %-6s | %-8s | %-34s │%n";
		System.out.format(leftAlignFormat3, "", "(*):Sho(w) All", "#:Set Row", "(x):Save", "0:Exit");
		
		System.out
				.println("└─────────────────────────────────────────────────────────────────────────────────────────────────────┘");

		System.out.println();
		if (showTip) {
			showShortCutTip();
		}
	}

	// *******************************************************************__Show_shortcut_tip__*************************************************************************

	private void showShortCutTip() {

		System.out.println("\t\t*** Tips (ShortCut) ***");
		System.out.format("%-7s %-30s%n", "ADD:",
				"(a-Title-Author-Content) use . to end statement !");
		System.out.format("%-7s %-30s%n", "Read:", "(r-id)");
		System.out
				.println("Update: (u-[id]-[title]-[author]-[content]) | (u-[id]-[t/a/c])");
		System.out.println("Delete: (d-[id]), to delete all enter \"dall\"");
		System.out.println("Search: (s-[keyword] | s-[i/t/a/c/d]-[keyword])");
	}

}
