package hrd.group3.article.layout;

public class Welcome extends Thread {
	public static void logo() {

		String[] animationChars = new String[] { "\t\t\t        ,--.",
				"                      ,--,\n", "", "", "", "\t\t\t    ,--/  /|",
				"   .--.--.          ", ",--.'|", " ,-.----.    ",
				"    ,---,\n", "\t\t\t ,---,': / '", "  /  /    '.",
				"     ,--,  | :", " \\    /  \\", "     .'  .' `\\\n",
				"\t\t\t :   : '/ /", "  |  :  /`. /", "  ,---.'|  : '",
				" ;   :    \\", "  ,---.'     \\\n", "\t\t\t |   '   ,",
				"   ;  |  |--`", "   |   | : _' |", " |   | .\\ :",
				"  |   |  .`\\  |\n", "\t\t\t '   |  /", "    |  :  ;_  ",
				"   :   : |.'  |", " .   : |: |", "  :   : |  '  |\n",
				"\t\t\t |   ;  ;", "     \\  \\    `.", "  |   ' '  ; :",
				" |   |  \\ :", "  |   ' '  ;  :\n", "\t\t\t :   '   \\",
				"     `----.   \\", " '   |  .'. |", " |   : .  /",
				"  '   | ;  .  |\n", "\t\t\t |   |    '", "    __ \\  \\  |",
				" |   | :  | '", " ;   | |  \\", "  |   | :  |  '\n",
				"\t\t\t '   : |.  \\", "  /  /`--'  /", " '   : |  : ;",
				" |   | ;\\  \\", " '   : | /  ;\n", "\t\t\t |   | '_\\.'",
				" '--'.     /", "  |   | '  ,/", "  :   ' | \\.'",
				" |   | '` ,/\n", "\t\t\t '   : |", "       `--'---'",
				"   ;   : ;--'", "   :   : :-'", "   ;   :  .'\n",
				"\t\t\t ;   |,'", "", "                  |   ,/",
				"       |   |.'", "     |   ,.'\n", "\t\t\t '---'", "",
				"                    '---'", "        `---'", "       '---'"

		};
		for (int i = 0; i < animationChars.length; i++) {
			System.out.print(animationChars[i]);
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println();
		try {
			Thread.sleep(700);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
