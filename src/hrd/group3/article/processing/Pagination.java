package hrd.group3.article.processing;

import hrd.group3.article.Article;
import hrd.group3.article.Main;
import hrd.group3.article.interfaceFile.PaginationInterface;
import hrd.group3.article.layout.Layout;
import hrd.group3.article.processing.FileProcess.ThreadWriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Pagination implements PaginationInterface {
	// Current ID from Class Article
	public static int current_ID = 0;

	// Pagination fields.
	private List<Article> list, temp;
	private int num_row = 5, current_page = 1;
	private int total_page;
	private boolean validInput = true;
	private Scanner sc = new Scanner(System.in);
	Layout layout = new Layout();
	Manipulation utility = new Manipulation();

	// Constructor
	public Pagination(List<Article> list) {
		this.list = list;
	}

	// ***************************************************__Navigation__***************************************************************
	@Override
	public void navigation() {
		try {
			String[] userInput;
			while (true) {
				System.out.print("\nChoose Options ->  ");
				userInput = Validation.chooseOption().split("-");

				// For normal input
				if (userInput.length == 1) {
					switch (userInput[0].toLowerCase()) {
					// First Page
					case "1":
					case "f":
						showFirstPage();
						break;
					// Previous Page
					case "2":
					case "p":
						showPreviousPage();
						break;
					// Next Page
					case "3":
					case "n":
						showNextPage();
						break;
					// Last Page
					case "4":
					case "l":
						showLastPage();
						break;
					// Show All Records
					case "5":
					case "w":
					case "*":
						current_page = 1;
						validInput = true;
						break;
					// Show Tips
					case "6":
					case "t":
						if (Layout.showTip)
							Layout.showTip = false;
						else
							Layout.showTip = true;
						break;
					// backup and restore
					case "7":
					case "b":
						System.out
								.print("Press b for backup and r for restore: ");

						switch (sc.nextLine()) {
						case "b":
							Validation.displayLoading("Data is backing up");
							FileProcess.backup();
							break;
						case "r":
							ArrayList<Article> checkList;
							// check when no backup file return null, then
							// nothing change
							if ((checkList = FileProcess.restore()) != null) {
								this.list = checkList;
								System.out.println("File is restored successful!");
								Validation.fileHasModified = true;
							}
							break;
						}
						break;
					// Read Record
					case "8":
					case "r":
						System.out.print("Enter id to read : ");
						utility.displayInfo(utility.search(this.list,
								Integer.parseInt(sc.nextLine())));
						System.out.print("\nPress any key to continue...");
						sc.nextLine();
						navigation();
						// Add Record
					case "9":
					case "a":
						if (utility.add(this.list))
							Validation.fileHasModified = true;
						validInput = true;
						break;
					// Search
					case "10":
					case "s":
						System.out.print("Enter id : ");
						Article a = utility.search(this.list,
								Integer.parseInt(sc.nextLine()));
						ArrayList<Article> t = new ArrayList<Article>();
						if (a != null)
							t.add(a);
						temp = t;
						validInput = false;
						break;
					// Update Record
					case "u":
					case "11":
						System.out.print("Enter ID to Update : ");
						utility.update(this.list,
								Integer.parseInt(sc.nextLine()));
						break;
					// Delete Record
					case "12":
					case "d":
						System.out.print("Enter ID to delete: ");
						utility.delete(this.list,
								Integer.parseInt(sc.nextLine()));
						validInput = true;
						break;
					// delete all record
					case "dall":
						if (current_ID != 0) {
							FileProcess.write_MAX_ID(current_ID);
						}
						if (Validation.checkConfrim("delete all record")) {
							Validation.displayLoading("Data is saving");
							this.list.clear();
							FileProcess.write(this.list);
							System.out.println("Done !!");
							Validation.fileHasModified = false;
						} else {
							System.out.println("Nothing is changed !");
							navigation();
						}
						break;
					// Go to Page
					case "13":
					case "g":
						goToPage();
						break;
					// Set Number of row
					case "#":
						setRowNumber();
						break;
					// Save to File
					case "x":
						if (current_ID != 0) {
							FileProcess.write_MAX_ID(current_ID);
						}
						if (Validation.fileHasModified) {
							if (FileProcess.ThreadState == "Terminated") {
								FileProcess.ThreadWriter writeFile = new FileProcess().new ThreadWriter(
										this.list);
								writeFile.start();
								Validation
										.displayLoading("Data is saving to database");
								LogFile lf = new LogFile();
								lf.addToLogFile();
								System.out.println("Done !!");
								Validation.fileHasModified = false;
							} else {
								System.err
										.print("Your previous saving has not complete, please wait a few secound and try to save again!! (>‿◠)✌ ");
								sc.nextLine();
							}
						} else {
							System.out.println("Nothing is changed !");
							navigation();
						}
						break;
					case "0":
					case "e":
						if (Validation.fileHasModified) {
							if (Validation
									.checkConfrim("save what you did so far")) {
								if (current_ID != 0) {
									FileProcess.write_MAX_ID(current_ID);
								}
								if (FileProcess.ThreadState == "Terminated") {
									FileProcess.ThreadWriter writeFile = new FileProcess().new ThreadWriter(
											this.list);
									writeFile.start();
									Validation
											.displayLoading("Data is saving to database");
									LogFile lf = new LogFile();
									lf.addToLogFile();
									writeFile.join();
								} else {
									System.err
											.print("Your previous saving has not complete, please wait a few secound and try to save again!! (>‿◠)✌ ");
									sc.nextLine();
								}
							}
						}

						System.out.println("\nGood Luck !!!");
						System.exit(0);
						// Invalid Input
					default:
						System.out.println("Invalid Input");
						navigation();
					}
				}
				// For shorthand input
				else if (userInput.length > 1) {
					switch (userInput[0].toLowerCase()) {
					// Add Record
					case "a":
						utility.add(this.list, userInput);
						Validation.fileHasModified = true;
						break;
					// Delete Record
					case "d":
						utility.delete(this.list, userInput);
						Validation.fileHasModified = true;
						break;
					// read Record
					case "r":
						utility.displayInfo(utility
								.search(this.list, userInput).get(0));
						System.out.print("\nPress any key to continue...");
						sc.nextLine();
						navigation();
						// Update Record
					case "u":
						utility.update(this.list, userInput);
						Validation.fileHasModified = true;
						break;
					// Search
					case "s":
						temp = utility.search(this.list, userInput);
						validInput = false;
						break;
					case "*":
						validInput = true;
						break;
					// Invalid Input
					default:
						System.err.println("Invalid Input");
						navigation();
					}
				}
				clearScreen(100);
				if (validInput) {
					showPage(this.list);
				} else
					showPage(temp);
			}
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Not Found !!");
			navigation();
		} catch (Exception e) {
			e.printStackTrace();
			// System.err.println("Error! Make sure your input is correct.");
			navigation();
		}

	}

	/************************** Sub Function ******************************/

	/**
	 * For Testing 10 Millions Records
	 * 
	 */
	final String[] Title = { "HRD", "Kosign", "Article", "Java", "SQL",
			"Sabay", "JavaScript", "VB.Net", "Ruby", "Advance C#", "HTML",
			"CSS" };
	final String[] Author = { "Yin Kokpheng", "Pirang", "Ho Sysome",
			"Ok Reskmey", "Seng Oudam" };
	final String[] Content = {
			"How to randomly pick an element from an array",
			" Random shuffling of an array",
			"How to generate a random alpha-numeric string?",
			" SQL Server 2005 freezes (because of application), need logging",
			" Join and update same column sql server",
			"C#: How to test for StackOverflowException",
			"Getting stackoverflowexception in ASP.NET MVC's Javascript Minifier at runtime" };
	Random random = new Random();

	public void addToListAuto(long amount) {
		for (int i = 1; i <= amount; i++) {
			this.list.add(new Article(Title[random.nextInt(Title.length)],
					Author[random.nextInt(Author.length)], Content[random
							.nextInt(Content.length)]));
		}
		Collections.sort(this.list, Collections.reverseOrder());
	}

	// ***************************************************__Set_Row_Number__***************************************************************
	@Override
	public void setRowNumber() {
		int back_up;
		System.out.print("Set Number of Row to Display : ");
		back_up = num_row;
		num_row = Integer.parseInt(sc.nextLine());
		if (num_row > this.list.size() || num_row <= 0) {
			num_row = back_up;
			System.out
					.println("The Number is bigger than existing records !\n");
			System.out.print("Press any key to continue...");
			sc.nextLine();
			navigation();
		}
		total_page = (int) Math.ceil((this.list.size() / (float) num_row));
		showFirstPage();
	}

	// ***************************************************__Goto_Page__***************************************************************
	@Override
	public void goToPage() {
		int back_up;
		System.out.print("Goto Page : ");
		back_up = current_page;
		current_page = Integer.parseInt(sc.nextLine());
		if (current_page > total_page || current_page <= 0) {
			current_page = back_up;
			System.err.println("There are only " + total_page + " pages !!!\n");
			System.out.print("Press any key to continue...");
			sc.nextLine();
			navigation();
		}
	}

	// ***************************************__Show_First_Page_&_Last_Page__*******************************************************
	@Override
	public void showFirstPage() {
		current_page = 1;
	}

	@Override
	public void showLastPage() {
		current_page = total_page;
	}

	// *****************************************__Show_Next_Page_&_Previous_Page__*******************************************************
	@Override
	public void showNextPage() {
		if (current_page == total_page) {
			System.out.println("Now on the Last Page !!!\n");
			System.out.print("Press any key to continue...");
			sc.nextLine();
			navigation();
		} else
			current_page += 1;
	}

	@Override
	public void showPreviousPage() {
		if (current_page == 1) {
			System.out.println("Now on the First Page !!!\n");
			System.out.print("Press any key to continue...");
			sc.nextLine();
			navigation();
		} else
			current_page -= 1;
	}

	// ***************************************************__Show_Page__***************************************************************
	@Override
	// current_page - 1 bcoz first page start with index 0
	public void showPage(List<Article> list) {
		total_page = (int) Math.ceil((list.size() / (float) num_row));
		int start = (current_page - 1) * num_row;
		int stop = start + num_row;

		if (current_page == total_page) {
			stop = list.size();
		}

		layout.Body(list, start, stop, current_page, total_page);
		layout.footer();
	}

	private void clearScreen(int rows) {
		for (int i = 0; i < rows; i++) {
			System.out.println();
		}
	}
}
