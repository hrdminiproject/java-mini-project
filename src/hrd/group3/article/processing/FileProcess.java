package hrd.group3.article.processing;

import hrd.group3.article.Article;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class FileProcess extends Thread {
	private List<Article> threadList;
	public static String ThreadState = "Terminated";

	// path of database file n id file
	private static String fileName = "data/db.txt";
	private static String fileId = "data/id.txt";

	/**
	 * Reads data from a file and return as a collection
	 * <tt>ArrayList of Article.</tt>
	 * */
	@SuppressWarnings("unchecked")
	public static ArrayList<Article> read() {
		ArrayList<Article> article = null;
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new BufferedInputStream(
					new FileInputStream(new File(fileName))));
			article = (ArrayList<Article>) ois.readObject();

		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found: " + e.getMessage());
		} finally {
			try {
				ois.close();
				return article;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return article;
	}

	/**
	 * Write collection to file
	 *
	 * <tt>ArrayList of Article</tt>
	 * */
	public static void write(List<Article> article) {
		ThreadState = "Running";
		ObjectOutputStream oos = null;
		try {
			// instantiate object
			oos = new ObjectOutputStream(new BufferedOutputStream(
					new FileOutputStream(new File(fileName))));
			// write object
			oos.writeObject(article);
		} catch (ConcurrentModificationException e) {
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				ThreadState = "Terminated";
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This class is extends Thread use for writing data in background
	 * 
	 * */
	public class ThreadWriter extends Thread {
		public ThreadWriter(List<Article> list) {
			threadList = list;
		}

		@Override
		public void run() {
			write(threadList);// write to file
		}
	}

	/**
	 * Read auto number
	 * */
	public static int read_MAX_ID() {
		FileInputStream fis = null;
		StringBuilder str = new StringBuilder("");
		char ch;
		try {
			try {
				fis = new FileInputStream(new File(fileId));
				while (fis.available() > 0) {
					ch = (char) fis.read();
					str.append(ch);
				}
			} catch (Exception e) {
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException ioe) {
				System.out.println("Error in InputStream close(): " + ioe);
			}
		}
		return Integer.parseInt(str.toString());
	}

	/**
	 * Write auto number
	 * */
	public static void write_MAX_ID(int id) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileId);
			fos.write(Integer.toString(id).getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException ioe) {
				System.out.println("Error in InputStream close(): " + ioe);
			}
		}
	}

	/**
	 * Check database file and id file if haven't create then create
	 * */
	public static void checkFile() {
		File fileDatabase = new File(fileName);
		File fileID = new File(fileId);
		File dir = new File("data");
		if (!dir.exists())
			dir.mkdirs();

		if (!fileDatabase.exists()) {
			try {
				fileDatabase.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (!fileID.exists()) {
			try {
				fileID.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * backup function
	 * */
	public static void backup() {
		InputStream inStream = null;
		OutputStream outStream = null;

		try {

			File db = new File(fileName);
			File dbBackup = new File(fileName.substring(0,
					fileName.length() - 4) + "-backup.txt");

			inStream = new FileInputStream(db);
			outStream = new FileOutputStream(dbBackup);

			byte[] buffer = new byte[1024];

			int length;
			while ((length = inStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, length);
			}

			inStream.close();
			outStream.close();

			System.out.println("File is backup successful!");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * restore function
	 * */
	public static ArrayList<Article> restore() {
		try {
			File db = new File(fileName);
			File dbBackup = new File(fileName.substring(0,
					fileName.length() - 4) + "-backup.txt");
			if (dbBackup.exists()) {
				if (db.delete()) {
					if (dbBackup.renameTo(db)) {
						Validation.displayLoading("Data is restoring");
						return read();
					} else {
						System.out.println("File restores failed!");
					}
				} else {
					System.out.println("Delete delete failed.");
				}
			} else {
				System.out.println("No Back-Up File to restore");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
