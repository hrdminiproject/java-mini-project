package hrd.group3.article.processing;

import java.util.Scanner;

public class Validation {
	static boolean fileHasModified = false;
	
	/**
	 * Validate the input, it is a number or not.
	 * 
	 * @param input
	 *            is the input string
	 * */
	static boolean isNumber(String input) {
		boolean i = false;
		if (!input.isEmpty()) {

			i = input.matches("[0-9]+");
		}
		return i;
	}

	/**
	 * Confirm user input again.
	 * 
	 * @param msg
	 *            display message.
	 */
	public static boolean checkConfrim(String msg) {
		Scanner sc = new Scanner(System.in);
		System.out.print("\nDo you want to " + msg + "? (Y/N): ");
		boolean choose = false;
		switch (sc.nextLine().toUpperCase()) {
		case "Y":
			choose = true;
			break;
		case "N":
			choose = false;
			break;
		default:
			checkConfrim(msg);
		}
		return choose;
	}

	/**
	 * This method is used for validating user input with multiple line texts.
	 * user need to add ` this symbol when complete.
	 */
	static String validateContent() {
		Scanner sc = new Scanner(System.in);
		StringBuffer sb = new StringBuffer();
		// If input is one character
		String imsi = sc.nextLine();
		// if input is more then one character
		while (true) {
			if (imsi != null && imsi.trim().charAt(0) == '.') {
				break;
			}
			if (imsi == null)
				imsi = "";

			sb.append(imsi + "}{");
			imsi = sc.nextLine();
		}
		return sb.toString();
	}

	/**
	 * This method is used for validating user choosing option from menu.
	 * <p>
	 * User can choose by input one character which is represent to any option,
	 * and they can use shorthand mode which input in one line then end with `
	 * symbol.
	 */
	static String chooseOption() {
		Scanner input = new Scanner(System.in);
		StringBuffer sb = new StringBuffer();

		// If input is one character
		String imsi = input.nextLine();
		String[] arr = imsi.split("-");
		if (imsi != null && imsi.trim().length() == 1) {
			return imsi.trim();
		}
		// u-id-title-author-content
		// a-title-author-content

		// u-content-id
		// s-id
		// s-keywords
		// s-t-(keyword)
		// r-id
		// d-id

		// if input is more then one character
		while (true) {
			if (imsi != null && imsi.trim().charAt(0) == '.') {
				break;
			}
			if (imsi == null)
				imsi = "";
			if (arr.length <= 3) {
				return imsi;
			}
			sb.append(imsi + "}{");
			imsi = input.nextLine();
		}
		return sb.toString();
	}

	static void displayLoading(String msg) {
		System.err.print(msg);
		for (int i = 0; i < 3; i++) {
			System.err.print(".");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("");
	}
}
