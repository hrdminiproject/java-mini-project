package hrd.group3.article.processing;

import hrd.group3.article.Article;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class LogFile {
	static ArrayList<String> listTransaction = new ArrayList<String>();

	/**
	 * add transaction to temporary listTransection
	 * */
	public void transact(String status, Article article) {
		listTransaction.add(getCurrentDate() + "\t\t" + status + "\t\t"
				+ article.getId());
	}

	private String getCurrentDate() {
		return new SimpleDateFormat("dd/MM/YYYY HH:mm:ss").format(new Date());
	}

	/**
	 * write temporary listTransection to file
	 * */
	public void addToLogFile() {
		checkLogFile();
		BufferedWriter bw = null;
		File file = new File("data/TempFile.log");
		Iterator<String> itr = listTransaction.iterator();
		try {
			bw = new BufferedWriter(new FileWriter("data/Log.log", true));
			while (itr.hasNext()) {
				bw.write(itr.next());
				bw.newLine();
			}
			listTransaction.clear();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
				if (file.exists())
					file.delete();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * check LogFile state is exist or not
	 * */
	private void checkLogFile() {
		File log = new File("data/Log.log");
		File dir = new File("data");
		if (!dir.exists())
			dir.mkdirs();

		if (!log.exists()) {
			try {
				log.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
