package hrd.group3.article.processing;

import hrd.group3.article.Article;
import hrd.group3.article.Main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;

public class TempFile {
	// Path
	// of
	// Temporary
	// File
	private final static String TEMPORARY_FILE_PATH = "data/TempFile.log";

	/**
	 * if Temporary File is not exists then create
	 * */
	private void createTemporaryFile() {
		File temp = new File(TEMPORARY_FILE_PATH);
		File dir = new File("data");
		if (!dir.exists())
			dir.mkdirs();

		if (!temp.exists()) {
			try {
				temp.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * write transaction to file
	 * */
	private void transact(String status, Article article) {
		createTemporaryFile();
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(TEMPORARY_FILE_PATH, true));
			switch (status) {
			case "Insert":
			case "Update":
				bw.write(status + "\t" + article.getArticle());
				break;
			case "Delete":
				bw.write("Delete\t" + article.getId());
				break;
			}
			bw.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void insert(Article article) {
		transact("Insert", article);
	}

	public void update(Article article) {
		transact("Update", article);
	}

	public void delete(Article article) {
		transact("Delete", article);
	}

	/**
	 * get Temporary file
	 * */
	public static File getTempFile() {
		File temp = new File(TEMPORARY_FILE_PATH);
		if (temp.exists()) {
			return temp;
		}
		return null;
	}

	/**
	 * Read from temporary file by case Insert, Update, Delete when read
	 * successfully then delete temporary file
	 * */
	public static void readFromTemporaryFile(File tempFile) {
		if (tempFile != null) {
			BufferedReader br = null;
			Article article = null;
			LogFile lf = new LogFile();
			try {
				br = new BufferedReader(new FileReader(tempFile));
				Manipulation m = new Manipulation();
				String[] content = null;
				String line = null;
				try {
					while ((line = br.readLine()) != null) {
						content = line.split("\t");
						switch (content[0]) {
						case "Insert":
							article = new Article(content[2], content[3],
									content[5]);
							article.setDate(content[4]);
							Main.articleList.add(0, article);
							FileProcess.write_MAX_ID(article.getId());
							lf.transact(content[0], article);
							break;
						case "Update":
							article = m.search(Main.articleList,
									Integer.parseInt(content[1]));
							int index = Main.articleList.indexOf(article);
							article.setTitle(content[2]);
							article.setAuthor(content[3]);
							article.setContent(content[4]);
							Main.articleList.set(index, article);
							lf.transact(content[0], article);
							break;
						case "Delete":
							article = m.search(Main.articleList,
									Integer.parseInt(content[1]));
							Main.articleList.remove(Main.articleList
									.indexOf(article));
							lf.transact(content[0], article);
							break;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
					FileProcess.ThreadWriter writeFile = new FileProcess().new ThreadWriter(
							Main.articleList);
					writeFile.start();
					lf.addToLogFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
