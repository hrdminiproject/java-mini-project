package hrd.group3.article.processing;

import hrd.group3.article.Article;
import hrd.group3.article.interfaceFile.ManipulationInterface;

import java.util.*;

public class Manipulation implements ManipulationInterface {
	Scanner sc = new Scanner(System.in);
	// Create array to store user input detail
	String[] arr = new String[4];

	// ********************************************__Display_Article_Information__*****************************************************
	@Override
	public void displayInfo(Article article) {
		if (article != null) {
			String content = article.getContent().replace("}{", "\n");

			// display data.
			System.out
					.println("\nID: " + article.getId() + "\nTitle: "
							+ article.getTitle() + "\nAuthor: "
							+ article.getAuthor() + "\nDate: "
							+ article.getDate() + "\nContents: " + content);
		} else {
			System.out.println("Your input is not found!!!\n");
		}
	}

	// ***************************************************__ADD_Date__***************************************************************
	@Override
	public boolean add(List<Article> list) {
		do {
			// ADD Title
			System.out.print("Title: ");
			arr[1] = sc.nextLine();

			// ADD Author
			System.out.print("Author: ");
			arr[2] = sc.nextLine();

			// ADD content
			System.out.print("Description: ");
			arr[3] = Validation.validateContent();

			// Check if user don't save then return
			if (add(list, arr) == null)
				return false;
		} while (Validation.checkConfrim("add more"));
		Collections.sort(list, Collections.reverseOrder());
		return true;
	}

	/**
	 * This method is used for adding a new article. <b>Shorthand used</b>
	 * 
	 * @param list
	 *            The list contains article.
	 * @param info
	 *            User's input info.
	 * 
	 * @return true when successfully add.
	 * 
	 */
	Article add(List<Article> list, String[] info) {
		// Shorthand format: a-title-author-content.
		// Ex: a-KSHRD Center-Kokpheng-Content: to end contents press enter > .

		// info[0] is a(shortcut)
		// info[1] is Title
		// info[2] is Author
		// info[3] is Description

		// Check if any fields is empty

		Article article = null;
		if (info[1].isEmpty() || info[2].isEmpty() || info[3].isEmpty()) {
			System.err.println("Please complete all the field!");
			return null;
		} else {
			// Create an object then add to arrayList
			article = new Article(info[1].trim(), info[2].trim(),
					info[3].trim());

			displayInfo(article);
			if (Validation.checkConfrim("insert this record")) {
				list.add(article);

				// write add to TempFile
				TempFile tf = new TempFile();
				LogFile lf = new LogFile();
				try {
					lf.transact("Insert", article);
					tf.insert(article);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Validation.displayLoading("Data is inserting");
			} else {
				article.decreaseAutoNumber();
				return null;
			}
		}
		Collections.sort(list, Collections.reverseOrder());
		return article;
	}

	// ***************************************************__Search_Article__***************************************************************
	@Override
	public Article search(List<Article> list, int id) {
		Iterator<Article> itr = list.iterator();
		Article result;
		while (itr.hasNext()) {
			result = itr.next();
			if (result.getId() == id) {
				return result;
			}
		}
		return null;
	}

	/**
	 * This method is used for searching an article by any string. <b>Shorthand
	 * used</b>.
	 * 
	 * @param list
	 *            The list contains article.
	 * @param info
	 *            User's input info.
	 * 
	 * @return return the objects that is found
	 * 
	 */
	ArrayList<Article> search(List<Article> list, String[] info) {
		Validation.displayLoading("Search Data");
		ArrayList<Article> tempList = new ArrayList<Article>();
		// find only ID.
		// s-(follow by number)
		if (info.length == 2 && Validation.isNumber(info[1])) {
			Article result = search(list, Integer.parseInt(info[1]));
			if (result != null)
				tempList.add(result);
		}
		// Find all include Title, Author, Contents from article.toString()'s
		// method.
		// s-(follow by keyword)
		else if (info.length == 2) {
			for (Article article : list) {
				if (article.toString().contains(info[1].trim().toLowerCase())) {
					tempList.add(article);
				}
			}
		}
		// Find by ID, Title, Author,Date, Contents depends on what user choose.
		else if (info.length > 2) {
			switch (info[1].toLowerCase()) {
			case "i":// find by ID
				for (Article article : list) {
					if (article.getId() == Integer.parseInt(info[2].trim())) {
						tempList.add(article);
						break;
					}
				}
				break;
			case "t":// find by Title
				for (Article article : list) {
					if (article.getTitle().toLowerCase()
							.contains(info[2].trim())) {
						tempList.add(article);
					}
				}
				break;
			case "a": // find by Author
				for (Article article : list) {
					if (article.getAuthor().toLowerCase()
							.contains(info[2].trim())) {
						tempList.add(article);
					}
				}
				break;
			case "d":// find by Date dd/mm/yyyy
				for (Article article : list) {
					if (article.getDate().toLowerCase()
							.contains(info[2].trim())) {
						tempList.add(article);
					}
				}
				break;
			case "c":// find by contents
				for (Article article : list) {
					if (article.getContent().toLowerCase()
							.contains(info[2].trim())) {
						tempList.add(article);
					}
				}
				break;
			}
		}

		return tempList;
	}

	// ***************************************************__Delete_Article__***************************************************************
	@Override
	public Article delete(List<Article> list, int id) {
		Article foundObject = search(list, id);
		if (foundObject != null) {
			displayInfo(foundObject);

			if (Validation.checkConfrim("delete")) {
				list.remove(foundObject);

				// write delete to TempFile
				TempFile tf = new TempFile();
				LogFile lf = new LogFile();
				try {
					lf.transact("Delete", foundObject);
					tf.delete(foundObject);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Validation.displayLoading("Data is deleting");
				Validation.fileHasModified = true;
				return foundObject;
			}
			else{
				Validation.fileHasModified = false;
			}
		} else {
			System.out.println("This ID is not found!!!");
			sc.nextLine();
		}
		return null;
	}

	/**
	 * This method is used for delete an article by id. <b>shorthand used</b>
	 * 
	 * @param list
	 *            target list.
	 * 
	 * @param info
	 *            the content that user input.
	 * 
	 * @return return the objects that has been remove.
	 * 
	 */
	Article delete(List<Article> list, String[] info) {
		return delete(list, Integer.parseInt(info[1].trim()));
	}

	// ***************************************************__Update_Article__***************************************************************
	@Override
	public Article update(List<Article> list, int id) {
		Article foundObject = search(list, id);
		if (foundObject != null) {
			arr[0] = foundObject.getTitle();
			arr[1] = foundObject.getAuthor();
			arr[2] = foundObject.getContent();

			while (true) {
				System.out
						.print("Choose:\t[1.Title]\t[2.Author]\t[3.Content] : ");
				switch (sc.nextLine()) {
				case "1":
					System.out.print("Enter Title: ");
					foundObject.setTitle(sc.nextLine());
					break;
				case "2":
					System.out.print("Enter Author: ");
					foundObject.setAuthor(sc.nextLine());
					break;
				case "3":
					System.out.print("Enter Content: ");
					foundObject.setContent(Validation.validateContent());
					break;
				default:

					System.out
							.println("\nYou have enter invalid option, please enter option again!!!\n");
					continue;
				}

				if (!Validation.checkConfrim("update other information"))
					break;
			}

			displayInfo(foundObject);

			if (Validation.checkConfrim("save")) {
				// write update to TempFile
				TempFile tf = new TempFile();
				LogFile lf = new LogFile();
				try {
					lf.transact("Update", foundObject);
					tf.update(foundObject);
					Validation.fileHasModified = true;
				} catch (Exception e) {
					e.printStackTrace();
				}
				Validation.displayLoading("Data is updating");
			} else {
				// don't save, reset everything to default
				foundObject.setTitle(arr[0]);
				foundObject.setAuthor(arr[1]);
				foundObject.setContent(arr[2]);
				Validation.fileHasModified = false;
			}

		} else {
			System.err.print("This ID is not found!");
			sc.nextLine();
		}
		return foundObject;
	}

	/**
	 * This method is used for update an article by <b>shorthand</b>.
	 * 
	 * @param list
	 *            target list.
	 * 
	 * @param info
	 *            user input information.
	 * 
	 * @return return the objects that has been update
	 * 
	 */
	// u-id-t-a-c
	Article update(List<Article> list, String[] info) {
		Article foundObject = new Article();
		// find object by id
		foundObject = search(list, Integer.parseInt(info[1].trim()));
		if (foundObject != null) {
			// User update all information
			if (info.length > 3) {
				// info[1] : input ID
				// info[2] : input Title
				// info[3] : input Author
				// info[4] : input Contents

				// store old information
				arr[0] = foundObject.getTitle();
				arr[1] = foundObject.getAuthor();
				arr[2] = foundObject.getContent();

				// set new information
				foundObject.setTitle(info[2]);
				foundObject.setAuthor(info[3]);
				foundObject.setContent(info[4]);

				displayInfo(foundObject);

				if (Validation.checkConfrim("save")) {
					// write update to TempFile
					TempFile tf = new TempFile();
					LogFile lf = new LogFile();
					try {
						lf.transact("Update", foundObject);
						tf.update(foundObject);
						Validation.fileHasModified = true;
					} catch (Exception e) {
						e.printStackTrace();
					}
					Validation.displayLoading("Data is updating");
				} else {
					foundObject.setTitle(arr[0]);
					foundObject.setAuthor(arr[1]);
					foundObject.setContent(arr[2]);
					Validation.fileHasModified = false;
				}
			}

			// u-t-id
			// User update Title, Author, Contents.
			else if (info.length == 3) {
				// store old information
				arr[0] = foundObject.getTitle();
				arr[1] = foundObject.getAuthor();
				arr[2] = foundObject.getContent();

				switch (info[2].toLowerCase()) {
				case "t": // Update title
					System.out.print("Please enter new title: ");
					foundObject.setTitle(sc.nextLine());
					break;
				case "a": // Update author
					System.out.print("Please enter new author: ");
					foundObject.setAuthor(sc.nextLine());
					break;
				case "c": // Update contents
					System.out.print("Please enter new content: ");
					foundObject.setContent(Validation.validateContent());
					break;
				default:
					System.err
							.println("\nError! Make sure your input is correct.");
					sc.nextLine();
					return foundObject;
				}

				displayInfo(foundObject);

				if (Validation.checkConfrim("save")) {
					// write update to TempFile
					TempFile tf = new TempFile();
					LogFile lf = new LogFile();
					try {
						lf.transact("Update", foundObject);
						tf.update(foundObject);
						Validation.fileHasModified = true;
					} catch (Exception e) {
						e.printStackTrace();
					}
					Validation.displayLoading("Data is updating");
				} else {
					foundObject.setTitle(arr[0]);
					foundObject.setAuthor(arr[1]);
					foundObject.setContent(arr[2]);
					Validation.fileHasModified = false;
				}
			} else {
				System.err.println("Error! Make sure your input is correct.");
				sc.nextLine();
			}
		} else {
			System.err.print("This ID is not found!");
			sc.nextLine();
		}
		return foundObject;
	}
}
